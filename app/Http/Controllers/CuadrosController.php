<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cuadro;
use App\Pintor;
use Illuminate\Support\Facades\Storage;

class CuadrosController extends Controller
{

	public function getCrear(){
		$pintores = Pintor::all();
		return view('cuadros.crear', array('pintores'=>$pintores));
	}
	public function postCrear(Request $request){

		$cuadro = new Cuadro();
		$cuadro->pintor_id= $request->pintor;
		$cuadro->nombre = $request->nombre ;
		$cuadro->imagen = $request->imagen->store('','cuadros');
		$cuadro->save();
		return redirect('pintores/mostrar/'.$request->pintor);
		
		try{
			$reserva->save();
			return redirect('pintores')->with('mensaje','Cuadro: '. $cuadro->nombre .' guardada');	
		}catch(\Illuminate\Database\QueryException $ex){
			return redirect('pintores')->with('mensaje','Fallo al crear Cuadro');
		}


		

	}

}

?>