@extends('layouts.master')
@section('titulo')
Listado de pintores
@endsection
@section('contenido')
@if (session('mensaje'))
<div class="alert alert-danger">
	{{session('mensaje')}}
</div>
@endif



<table>
	<tr><th><strong>Nombre</strong></th><th><strong>Pais</strong></th><th><strong>Cuadros</strong></th></tr>
	@foreach( $pintores as $pintor )

	<tr>
		<td>
			<a href="{{ url('/pintores/mostrar/') }}/{{$pintor->id}}">
				{{$pintor->nombre}}
			</a>

		</td>
		<td>
			{{$pintor->pais}}


		</td>
		<td>
			{{$pintor->cuadros->count()}}
		</td>
	</tr>

	@endforeach

</table>



@endsection


