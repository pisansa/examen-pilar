<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-secondary">
  <a class="navbar-brand" href="{{url('/')}}">Pinacoteca</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a href="{{url('/pintores')}}" class="nav-link {{ Request::is('pintores*') && !Request::is('cuadros/crear')? ' active' : ''}}">Todos los pintores</a>
      </li>
      <li class="nav-item">
        <a href="{{url('/cuadros/crear')}}" class="nav-link {{ Request::is('cuadros/crear')? ' active' : ''}}">Nuevo cuadro</a>
      </li>
    </ul>
    
  </div>
</nav>





