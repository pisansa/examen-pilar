
@extends('layouts.master')
@section('titulo')
Crear Cuadro
@endsection
@section('contenido')
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">
				Crear cuadro
			</div>
			<div class="card-body" style="padding:30px">

				<form action="{{ action('CuadrosController@postCrear') }}" method="POST" enctype="multipart/form-data">
					
					{{ csrf_field() }}
					<div class="form-group">
						<label for="nombre">Nombre</label>
						<input type="text" name="nombre" id="nombre" class="form-control">
					</div>
					<div class="form-group">
						<select name="pintor">
							@foreach ($pintores as $pintor)
							<option value="{{$pintor->id}}">{{$pintor->nombre}}</option>
							@endforeach
						</select>
					</div>
					
					<div class="form-group">
						{{-- TODO: Completa el input para la imagen --}}
						<label for="imagen">Subir foto</label>
						<input type="file" name="imagen" id="imagen" class="form-control">

					</div>
					
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
							Subir cuadro
						</button>
					</div>
					{{-- TODO: Cerrar formulario --}}
				</form>
			</div>
		</div>
	</div>
</div>
@endsection























